/** Need this global to close info windows */
var infoWindow;

/**
* This function is called when the asyncronous call to the GMAPS API is called sometime during  page load
* It initalizes the google maps api and loads the map with a stating marker
* I stole some of this from google documentation
* @param {string} beginString - String representation of the begining of the date range to pull events from
* @param {string} endString - String representation of the end of the date range to pull events from
*/
function initMap(beginString, endString)
{
	var austin = {lat: 30.2672, lng: -97.7431};
	var map = new google.maps.Map(document.getElementById('map'),
	{
		zoom: 10,
		center: austin
	});
	var requestData = {beginDate:beginString, endDate:endString};
	$.ajax
	({
		async: true,
		dataType: "json",
		data: requestData,
		url: "map.php",
		success: function( data, s )
		{
			var marker;
			console.log(data);
			for (var ndx in data)
			{
				var latitude = Number(data[ndx].latitude);
				var longitude = Number(data[ndx].longitude);
				var coords = new google.maps.LatLng(latitude, longitude);
				var startArray = data[ndx].start_date_time.split(" ");
				//var autocomplete = new google.maps.places.Autocomplete(data[ndx].address1);
				//console.log(autocomplete);
				//var place = autocomplete.getPlace();
				//console.log(place);
				//var placeRequest = {
  					//placeId: place.place_id
				//};
				//service = new google.maps.places.PlacesService(map);
				//service.getDetails(request, function(place, status)
				//{
  					//if (status == google.maps.places.PlacesServiceStatus.OK) 
					//{
						//console.log(place);
    					//createMarker(place);
  					//}
				//});
				var content = "<h2>"+startArray[0]+"</h2>"+"<h3>"+data[ndx].title+"</h3>"+"<p>"+data[ndx].address1+"</p>"+"<p>"+data[ndx].description+"</p>";
				console.log(coords);
				console.log(data[ndx]);
				//addMarker(map, data[ndx].title, coords);
				addMarker(map, content, coords);
			}
		}
	});
}

/**
* This will create the marker and attach it to the map
* This is also responsible for creating the info windows and attaching the event listener for the info window
* @param {object} map - Google map object to attach marker to
* @param {string} content - Event title
* @param {object} coords - Coordinte object where marker should be placed on map
*/
function addMarker(map, content, coords)
{
	var marker = new google.maps.Marker
	({
		position: coords,
		map: map
	});

	google.maps.event.addListener(marker, 'click', function()
	{
		if(typeof infoWindow != 'undefined')
		{
			console.log(infoWindow);
			infoWindow.close();
		}
		//infoWindow = new google.maps.InfoWindow({content: title});
		infoWindow = new google.maps.InfoWindow({content: content});
		infoWindow.open(map,marker);
	});
}

/**
* Nothing in here but the document ready function (it has its own comment block)
*/
$(document).ready
(
	/**
	* This is the main function called when the DOM becomes available for manipulation
	* No parameters
	*/
	function()
	{
		/**
		* Attach date picker functionality to date inputs
		* I would have used a class like "dateInput" if I had not had to set two different dates to start
		*/
		// Today
		var todayObj = new Date();
		var dateString = todayObj.getMonth()+1 + '/' + todayObj.getDate() + '/' + todayObj.getFullYear();
		$( "#datepickerStart" ).datepicker();
		$( "#datepickerStart" ).datepicker("setDate", dateString);

		// One month from Today
		var dateString = todayObj.getMonth()+2 + '/' + todayObj.getDate() + '/' + todayObj.getFullYear();
		$( "#datepickerEnd" ).datepicker();
		$( "#datepickerEnd" ).datepicker("setDate", dateString);

		// An event handler which listens for the date range change
		$( "#datepickerEnd" ).on("change", function()
		{
			var startDate = $("#datepickerStart").datepicker({dateFormat: 'dd/mm/yyyy'}).val();
			var endDate = $("#datepickerEnd").datepicker({dateFormat: 'dd/mm/yyyy'}).val();
			initMap(startDate, endDate);
		});

		// This is hacky and given more time I would reconsider my approach to events
		$( "#datePickerEnd" ).datepicker({onSelect: function(dateText, inst)
		{
			$(this).trigger('change');
		}});
	}
);

