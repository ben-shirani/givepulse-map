<?php

error_reporting(E_ALL);
require_once("map.inc.php");
require_once("map.class.php");

/**
 * Controller logic / application logic
 * With more time I would have put this in a separate file for better MVC compliance
 */
if(isset($_REQUEST['beginDate']) && !empty($_REQUEST['beginDate']) && is_string($_REQUEST['beginDate']) && isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) && is_string($_REQUEST['endDate']))
{
	$beginDate = trim($_REQUEST['beginDate']);
	$endDate = trim($_REQUEST['endDate']);
	$map = new Map($dbHost, $dbName, $dbUsername, $dbPassword);
	echo $map->pullEventsByDateRange($beginDate, $endDate);
}

?>
