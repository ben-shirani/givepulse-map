<?php

/**
 * This is the Map Class.
 * Time permitting I would have separated some of this out into more appropriate data containers.
 * For example I would have prefered to have an event class, cause class, and maybe even a location class.
 * Since time is a factor and the scope of the project is limited I bent the rules of design a little and approached the problem from the perspective of a single object: a map and data that is associated with a map.
 * 
 * @author Hassan Shirani <ben.shirani@gmail.com>
 *
 * @version 0.1
 *
 * @since 0.1
**/
class Map
{
	private $e; 
	private $db;
	private $dbHost = "";
	private $dbName = "";
	private $dbUsername = "";
	private $dbPassword = '';

	/**
	 * This is the constructor. It configures and establishes the database connection
	 * @param float $centerLat Latatude coordinate of where the map should be centered, defaults to austin
	 * @param float $centerLong Longitude coordinate of where the map should be centered, defaults to austin
	 * @param string $beginDate String representation ("MM/DD/YYYY") of the begining of date range to select events to display on map
	 * @param string $endDate String representation ("MM/DD/YYYY") of the end date of the date range to select events to display on map
	 * @param string $originAddress String representation of an address ("XXXX Street, City, XXXXX") to find events near
	 * @param int $radiusInMiles The number of miles from the starting point the map should select events within
	**/
	//public function __construct($centerLat = NULL, $centerLong = NULL, $beginDate = NULL, $endDate = NULL, $originAddress = NULL, $radiusInMiles = NULL)
	public function __construct($dbHost, $dbName, $dbUsername, $dbPassword)
	{
		try
		{
			// Set these to the object and use this line if moving to a different machine
			//$this->db = new PDO("mysql:dbname=$this->dbName;host=$this->dbHost",$this->dbUsername, $this->dbPassword);
			$this->db = new PDO("mysql:dbname=$dbName;host=$dbHost",$dbUsername, $dbPassword);
		}
		catch (Exception $e)
		{
			$this->e = $e;
			//echo "Exception: ", $e->getMessage(), "\n";
		}
	}

	/**
	 * This function will pull the data from the database
     * time permitting this would be separated out into more data specific accessors
	 *
	 * @param float $beginDate Start of date range to pull events
	 * @param float $endDate End of date range to pull events
	 */
	public function pullEventsByDateRange($beginDate = NULL, $endDate = NULL)
	{
		$beginDateFormatted = $this->formatDateForQuery($beginDate);
		$endDateFormatted = $this->formatDateForQuery($endDate);
		$sql = "SELECT 
					e.title,
					e.latitude,
					e.longitude,
					e.address1,
					e.description,
					e.start_date_time
				FROM 
					events AS e 
				WHERE 
					e.start_date_time
						BETWEEN
							?
						AND
							?
				AND
					e.start_date_time != '0000-00-00 00:00:00'
				";
				//LEFT JOIN events_causes_rel AS ecr ON e.id = ecr.event_id
				//LEFT JOIN causes AS c ON ecr.cause_id = c.id
				//LEFT JOIN groups_causes_rel AS gcr ON c.id = gcr.cause_id
				//LEFT JOIN groups AS g ON gcr.group_id = g.id
		$query = $this->db->prepare($sql);
		$query->execute(array($beginDateFormatted, $endDateFormatted));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		foreach ($result as $rowKey => $row)
		{
			foreach ($row as $columnKey => $column)
			{
				$result[$rowKey][$columnKey] = utf8_encode($column);
				//$returnObj[] = $row;
			}
		}
		return json_encode($result);
		//return json_encode($returnObj);
	}

	/**
     *
	 * Formats a date so that it is suitable for use in a mysql query
	 * @param string $htmlDate Date in HTML common use ("MM/DD/YYYY")
	 * Ideally this would accept three different inputs: month, day, and year and return the formatted date string
	 *
	 */
	public function formatDateForQuery($htmlDate)
	{
		// Sanity Check (excessive)
		if (!empty($htmlDate) && is_string($htmlDate) && !is_array($htmlDate))
		{
			$dateArray = explode("/", $htmlDate);
			$dateString = $dateArray[2]."-".$dateArray[0]."-".$dateArray[1]." "."00:00:00";
			return $dateString;
		}
	}
}

?>
